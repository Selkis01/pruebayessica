<?php  require_once 'global.php'; ?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/style.css">
</head>
<body>

<form class="modal-content animate" action="controller/RegisterController.php" method="POST">
    <div class="container">
      <?php 
        get_msg();
      ?>
      <h2>Registro</h2>
      <label for="user"><b>Nombre</b></label>
      <input type="text" placeholder="Ingrese Nombre" name="name" required>

      <label for="user"><b>Username</b></label>
      <input type="text" placeholder="Ingrese Username" name="user" required>

      <label for="password"><b>Contraseña</b></label>
      <input type="password" placeholder="Ingrese Contraseña" name="password" required>
        
      <button type="submit">Guardar</button>
      <button type="button" class="btn-red"  onclick="window.location='index.php'">Cancelar</button>
    </div>
</form>

<script type="text/javascript" src="js/main.js"></script>
</body>
</html>

