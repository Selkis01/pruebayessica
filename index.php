
<?php  require_once 'global.php'; ?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/style.css">
</head>
<body>

<form class="modal-content animate" action="controller/loginController.php" method="post">
 

    <div class="container">
    	<?php 
	        get_msg();
	    ?>

      <label for="user"><b>Username</b></label>
      <input type="text" placeholder="Enter Username" name="user" required>

      <label for="password"><b>Password</b></label>
      <input type="password" placeholder="Enter Password" name="password" required>
        
      <button type="submit">Login</button>
      <button type="button" class="btn-blue"  onclick="window.location='register.php'">Registrar</button>
    </div>
</form>

<script type="text/javascript" src="js/main.js"></script>
</body>
</html>


