<?php  require_once 'global.php'; ?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/style.css">
</head>
<body>

<form class="modal-content animate" action="controller/GoodsController.php" method="POST">
    <div class="container">
      <?php 
        get_msg();
      ?>
      <h2>Edicion</h2>
      <input type="hidden" name="update" value="update">
      <input type="hidden" name="id" value="<?php  echo $_GET['id']; ?>">
      <label for="user"><b>Nombre</b></label>
      <input type="text" placeholder="Ingrese Nombre" name="name" required value="<?php  echo $_GET['name']; ?>">

      <label for="user"><b>description</b></label>
      <input type="text" placeholder="Ingrese description" name="description" required  value="<?php  echo $_GET['description']; ?>">

      <label for="password"><b>value</b></label>
      <input type="text" placeholder="Ingrese value" name="value" required  value="<?php  echo $_GET['value']; ?>">
        
      <button type="submit">Guardar</button>
      <button type="button" class="btn-red"  onclick="window.location='../home.php'">Cancelar</button>
    </div>
</form>

<script type="text/javascript" src="js/main.js"></script>
</body>
</html>