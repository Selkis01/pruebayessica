<?php

require_once "BaseDatos.php";

class Goods
{
	protected $conexion;
	protected $db;

	function __construct(){
		$this->db = new BaseDatos();
		if($this->db->conectar()){
		    $this->conexion = $this->db->getConexion();
		}
	}

	public function get(){
		try {
			$arr = array();
			$sentencia = $this->conexion->prepare("SELECT * FROM goods");
	        $sentencia->execute();
	        $result = $sentencia->get_result();
	        if ($result->num_rows > 0) {
	        	while($row = $result->fetch_assoc()) {
				  $arr[] = $row;
				}
	        }
	        return $arr;

		} catch (Exception $e) {
			error_log($e);
			return $e->message();
		}
	}

	public function insert($name, $description, $value){
		try {
			$sentencia = $this->conexion->prepare("INSERT INTO goods (name, description, value) VALUES (?, ?, ?)");
			$sentencia->bind_param("sss", $name, $description, $value);
	        $sentencia->execute();
	        return $this->conexion->insert_id;
		} catch (Exception $e) {
			error_log($e);
			return $e->message();
		}
	}

	public function update($id, $name, $description, $value){
		try {
			$sentencia = $this->conexion->prepare("UPDATE goods SET name = ?, description = ?, value = ? WHERE id = ?");
			$id = (int)$id;
			$sentencia->bind_param("sssi", $name, $description, $value, $id);
	        $sentencia->execute();
	        if ($sentencia->affected_rows > 0) {
	        	return true;
	        }
	        return false;
		} catch (Exception $e) {
			error_log($e);
			return $e->message();
		}
	}

	public function delete($id){
		try {
			$sentencia = $this->conexion->prepare("DELETE FROM goods WHERE id = ?");
			$sentencia->bind_param("i", $id);
	        $sentencia->execute();
	        if ($sentencia->affected_rows > 0) {
	        	return true;
	        }
	        return false;
		} catch (Exception $e) {
			error_log($e);
			return $e->message();
		}
	}


	function __destruct() {
    	$this->db->desconectar();
    }
}