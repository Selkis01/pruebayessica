<?php

require_once "BaseDatos.php";
require_once __DIR__."/../config.php";

class User
{

	protected $conexion;
	protected $db;
	protected $user;
	protected $salt;

	function __construct(){
		$this->db = new BaseDatos();
		if($this->db->conectar()){
		    $this->conexion = $this->db->getConexion();
		}
	}

	public function login($user, $password){
		try {
			$password = $this->getPassword($password);
	        $sentencia = $this->conexion->prepare("SELECT id, name, user FROM users WHERE user = ? AND password = ?");
	        $sentencia->bind_param("ss", $user,$password);
	        $sentencia->execute();
	        $result = $sentencia->get_result();
	        if ($result->num_rows > 0) {
	        	$sentencia->bind_result($id, $name, $user);
				$sentencia->fetch();
				$this->user = ['id' => $id, 'name' => $name, 'user' => $user];
				$this->setSession();
	        }
	        return ($result->num_rows > 0) ? true : false ;
		} catch (Exception $e) {
			error_log($e);
			return false;
		}
		
    }

    public function logaut(){
		session_unset(); 
		session_destroy();
    }

    public function register($name, $user, $password){

    	try {
    		$password = $this->getPassword($password);
    		if ($this->validateUser($user)  < 1) {
	    		$sentencia = $this->conexion->prepare("INSERT INTO users (name, user, password) VALUES (?, ?, ?)");
		        $sentencia->bind_param("sss", $name, $user, $password);
		        $sentencia->execute();
		        return $this->conexion->insert_id;
	    	}
	    	return false;
    	} catch (Exception $e) {
    		$this->conexion->rollback();
    		error_log($e);
    		return $e->message();
    	}
    	
    }

    public function getUser(){
    	return $this->user;
    }

    protected function setSession(){
    	$_SESSION["login"] = true;
    	$_SESSION["user"] = $this->getUser();
    }

    protected function validateUser($user){

    	try {
    		$sentencia = $this->conexion->prepare("SELECT * FROM users WHERE user = ?");
	        $sentencia->bind_param("s", $user);
	        $sentencia->execute();
	        $result = $sentencia->get_result();
	        return ($result->num_rows > 0) ? 1 : 0;
    	} catch (Exception $e) {
    		error_log($e);
    		throw $e;
    	}
    	
    }

    protected function getPassword($password){
    	return md5(md5($password.SALT));
    }

    function __destruct() {
    	$this->db->desconectar();
    }
}
