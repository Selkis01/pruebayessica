<?php

require_once __DIR__."/../config.php";

class BaseDatos
{
    protected $conexion;
    protected $db;

    public function conectar()
    {
        $this->conexion = mysqli_connect(HOST, USER, PASS, DBNAME);
        if ($this->conexion->connect_error) {
            echo "Error de Connexion ($link_id->connect_errno)
            $link_id->connect_error\n";
            header('Location: error-conexion.php');
        exit;
        }

        return true;
    }

    public function getConexion()
    {
        return $this->conexion;
    }

    public function desconectar()
    {
        if ($this->conexion) {
            mysqli_close($this->conexion);
        }

    }

    // public function login(){
    //     $sentencia = $this->conexion->prepare("SELECT * FROM goods WHERE id = ?");
    //     $sentencia->bind_param("i", $id_usuario );
    //     $sentencia->execute();
    // }


    // public function pruebadb()
    // {
    //    if ($resultado =  $this->conexion->query("SELECT * FROM goods LIMIT 10")) {
    //         printf("La selección devolvió %d filas.\n", $resultado->num_rows);
    //         $resultado->close();
    //     } 
    // }
}