<?php  require_once 'global.php'; ?>
<?php  require_once __DIR__.'/controller/GoodsController.php'; ?>
<?php  $good = new GoodsController(); ?>
<?php  validateSession(); ?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<div class="container">
		<?php 
	        get_msg();
	    ?>
	    <form action="controller/LoginController.php" method="GET">
	    	<input type="hidden" name="salir" value="salir">
			<button type="submit">Salir de la App</button>
		</form>
		<h2>Goods</h2>
		<button type="button" onclick="window.location='create.php'">Crear Nuevo</button>
		<table class="table">
			<thead>
				<tr>
					<th>Nombre</th>
					<th>Description</th>
					<th>Valor</th>
					<th colspan="2">Acciones</th>
				</tr>
			</thead>
			<tbody>
				<?php if(count($good->index()) > 0 ) { ?>
					<?php foreach ($good->index() as $key) { ?>
						<tr>
							<td><?php echo $key["name"] ?></td>
							<td><?php echo $key["description"] ?></td>
							<td><?php echo $key["value"] ?></td>
							<td>
								<form action="controller/GoodsController.php" method="DELETE">
									<input type="hidden" name="id" value="<?php echo $key['id'] ?>">
									<input type="hidden" name="delete" value="delete">
									<button type="submit" class="btn-red">Eliminar</button>
								</form>
							</td>
							<td>
								<form >
								<button type="button" class="btn-blue" onclick="window.location='edit.php?id=<?php echo $key['id'] ?>&name=<?php echo $key['name'] ?>&description=<?php echo $key['description'] ?>&value=<?php echo $key['value'] ?>'">Editar</button>
								</form>
							</td>
						</tr>
					<?php } ?>
				<?php } ?>
			</tbody>
		</table>
	</div>

<script type="text/javascript" src="js/main.js"></script>
</body>
</html>