<?php

require_once __DIR__."/../BD/Goods.php";
require_once __DIR__.'/../global.php';

class GoodsController
{

	private $model;
	private $name;
    private $description;
    private $value;
    private $id;

	function __construct(){
		$this->model = new Goods();
        $this->id = isset($_GET['id']) ? $_GET['id'] : null;
        $this->idU = isset($_POST['id']) ? $_POST['id'] : null;
		$this->name = isset($_POST['name']) ? $_POST['name'] : null;
        $this->description = isset($_POST['description']) ? $_POST['description'] : null;
        $this->value = isset($_POST['value']) ? $_POST['value'] : null;

	}

    function index() {
       return $this->model->get();
    }

	function start() {
        if (empty($this->name) || empty($this->description) || empty($this->value)) {
            set_msg('Datos Inválidos', "danger");
        	header('Location: ../home.php');
        }else{
        	if ($this->model->insert($this->name, $this->description, $this->value) > 0) {
        		set_msg('Guardado Exitoso', "success");
            	header('Location: ../home.php');
        	}else{
        		set_msg('Datos Inválidos', "danger");
        		header('Location: ../home.php');
        	} 
        }
    }

    function update() {
        
        if (empty($this->name) || empty($this->idU) || empty($this->description) || empty($this->value)) {
            set_msg('Datos Inválidos', "danger");
            header('Location: ../home.php');
        }else{
            if ($this->model->update($this->idU, $this->name, $this->description, $this->value)) {
                set_msg('Actualizacion Exitosa', "success");
                header('Location: ../home.php');
            }else{
                set_msg('Datos Inválidos', "danger");
                header('Location: ../home.php');
            } 
        }
    }

    function delete() {
        if (empty($this->id)) {
            set_msg('Datos Inválidos', "danger");
            header('Location: ../home.php');
        }else{
            if ($this->model->delete($this->id)) {
                set_msg('Eliminacion Exitosa', "success");
                header('Location: ../home.php');
            }else{
                set_msg('Datos Inválidos', "danger");
                header('Location: ../home.php');
            } 
        }
    }

}

$login = new GoodsController();
if(!empty($_POST))
{
    if(isset($_POST['update'])){
        $login->update(); 
    }else{
        $login->start();
    }
}

if(!empty($_GET))
{
    $login->delete(); 
}

// $method = $_SERVER['REQUEST_METHOD'];
// switch ($method) {
//     case 'POST':
//         $login->start();
//         break;
//     case 'GET':
//         if(isset($_POST['delete'])){
//             $login->delete(); 
//         }else{
//             $login->update();
//         }  
//         break;
//     default:
//         $login->index();
//         break;
// }

