<?php

require_once __DIR__."/../BD/User.php";
require_once __DIR__.'/../global.php';

class LoginController
{

	private $model;
	private $user;
    private $password;

	function __construct(){
		$this->model = new User();
		$this->user = isset($_POST['user']) ? $_POST['user'] : null;
        $this->password = isset($_POST['password']) ? $_POST['password'] : null;
	}

	function start() {
        if (empty($this->user) || empty($this->password)) {
            set_msg('Datos Inválidos', "danger");
        	header('Location: ../index.php');
        }else{
        	if ($this->model->login($this->user, $this->password)) {
        		set_msg('Login Exitoso', "success");
            	
        	}else{
        		set_msg('Datos Inválidos', "danger");
        		header('Location: ../index.php');
        	} 
        }
    }

    function salir() {
    	echo "string";
    	if ($this->model->logaut()) {
    		set_msg('Login Exitoso', "success");
        	
    	}else{
    		set_msg('Datos Inválidos', "danger");
    		header('Location: ../index.php');
    	} 
    }
}

$login = new LoginController();
if(!empty($_POST))
{
    $login->start();
}

if(!empty($_GET))
{
    echo "string";
    $login->salir();
}

