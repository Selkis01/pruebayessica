<?php

require_once __DIR__."/../BD/User.php";
require_once __DIR__.'/../global.php';

class RegisterController
{

	private $model;
	private $user;
    private $password;
    private $name;

	function __construct(){
        session_start();
		$this->model = new User();
		$this->user = isset($_POST['user']) ? $_POST['user'] : null;
        $this->password = isset($_POST['password']) ? $_POST['password'] : null;
        $this->name = isset($_POST['name']) ? $_POST['name'] : null;
	}

	function start() {
        if (empty($this->user) || empty($this->password)|| empty($this->name)) {
            set_msg('Datos Inválidos', "danger");
            header('Location: ../register.php');
        }else{
            if ($this->model->register($this->name, $this->user, $this->password) > 0) {
                if ($this->model->login($this->user, $this->password)) {
                    set_msg('Resgistro Exitoso', "success");
                    header('Location: ../home.php');
                };
            }else{
                set_msg('El usuario ya existe', "danger");
                header('Location: ../register.php');
            }
        }
    }
}

$controller = new RegisterController();

if(!empty($_POST))
{
    
    $controller->start();
}
